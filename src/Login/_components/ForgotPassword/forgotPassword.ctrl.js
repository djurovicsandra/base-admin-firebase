import firebase from "firebase";
import {required, email} from 'vuelidate/lib/validators'

export default {
    data() {
        return {
            imageUrl: `${process.env.ASSETS_URL}/images`,
            userData: {
                username: ''
            },
            error: null
        };
    },
    validations: {
        userData: {
            username: {required, email},
        }
    },
    computed: {
        usernameErrors() {
            const errors = [];
            if (!this.$v.userData.username.$dirty) return errors;
            !this.$v.userData.username.required && errors.push('Email is required');
            !this.$v.userData.username.email && errors.push('Email must be in valid format');
            return errors
        },
    },
    methods: {
        submit() {
            let _this = this;
            firebase.auth().sendPasswordResetEmail (this.userData.username)
                .then(function () {
                    alert('Password reset email sent, check your inbox.');
                    _this.$router.push('/login');
            }).catch((error) => {
                window.alert(error)
            })
        }
    }
};

