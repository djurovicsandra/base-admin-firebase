import firebase from "firebase";
import {required, email, minLength, maxLength} from 'vuelidate/lib/validators'

export default {
    data() {
        return {
            imageUrl: `${process.env.ASSETS_URL}/images`,
            userData: {
                username: '',
                password: '',
            },
            error: null
        };
    },
    validations: {
        userData: {
            username: {required, email},
            password: {required, minLength: minLength(6), maxLength: maxLength(16)},
        }
    },
    computed: {
        usernameErrors() {
            const errors = [];
            if (!this.$v.userData.username.$dirty) return errors;
            !this.$v.userData.username.required && errors.push('Username is required');
            !this.$v.userData.username.email && errors.push('Username must be in valid format');
            return errors
        },
        passwordErrors() {
            const errors = [];
            if (!this.$v.userData.password.$dirty) return errors;
            !this.$v.userData.password.required && errors.push('Password is required');
            !this.$v.userData.password.minLength && errors.push('Minimum password length is 6 characters');
            !this.$v.userData.password.maxLength && errors.push('Maximum password length is 16 characters');
            return errors
        }
    },
    methods: {
        submit() {
            firebase.auth().signInWithEmailAndPassword(this.userData.username, this.userData.password)
                .then(data => {
                    // localStorage.setItem('firstname', data.user.displayName);
                    localStorage.setItem('username', data.user.email);
                    this.$router.push('/dashboard')
                })
                .catch(err => {
                    this.error = err.message;
                });
        },
        toRegister() {
            this.$router.push('/register')
        },
        toForgotPassword() {
            this.$router.push('/forgot-password')
        }
    }
};
