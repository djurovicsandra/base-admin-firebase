import {API_URL} from '../app-axios.js'

const api_path = '/projects';

const state = {
  seo: {
    title: 'Single Project Title',
    description: 'Single Project description'
  },
  items: [],
  loadingStatus: ''
}

// getters
const getters = {
  items: (state, getters) => {
    return state.items;
  },
  loadingStatus: (state) => {
    return state.loadingStatus; 
  },
  seo: (state) => {
    return state.seo;
  }
}

// actions
const actions = {
  seo({commit}) {
    commit('seo/SET_SEO', state.seo, {root: true})
  },

  getProject({commit}, data) {
    commit('SET_STATUS', 'LOADING'),
    API_URL.get(api_path + '/' + data)
      .then(response => (
          commit('SET_STATUS', 'LOADED'),
          setTimeout(function(){
            commit('SET_STATUS', 'READY')
          }, 1000),

          commit('SET_ITEMS', response)
      )
    )
    .catch(error => (
        // console.log('error: ', error),
            commit('SET_STATUS', 'ERROR')
        )
    )
  }
}

// mutations
const mutations = {
  SET_STATUS (state, data) {
    // console.log('loadingStatus: ' + data);
    state.loadingStatus = data;
  },

  SET_ITEMS (state, data) {
    state.items = data.data;
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}