import {SEOResource} from '../app-seo.js'

export default {
    name: 'home',
    
    data () {
        return {
            info: null,
            show: false,
            color: '#000'
        }
    },

    computed: {
        loading: {
            get: function () {
                let _ls = this.$store.getters['home/loadingStatus'];
                this.show = _ls === 'LOADED' || _ls === 'READY' ? true : false;
                this.color = _ls === 'LOADED' ? '#008000' : '#000'
                return _ls;
            },
            set: function (newValue) {}
        }
    },

    mounted () {
        // this.$store.dispatch('home/setStatus');
        this.$store.dispatch('home/seo');
        this.info = this.$store.getters['home/items'];   
    },

    metaInfo () {
        return new SEOResource().SEOData(this);
    }
};