import firebase from "firebase";

export default {
    name: 'header',
    data() {
        return {
            imageUrl: `${process.env.ASSETS_URL}/images`,
        }
    },
    mounted() {
        // this.$store.dispatch('header/getMenu');
    },
    methods: {
        logout() {
            firebase
                .auth()
                .signOut()
                .then(() => {
                    localStorage.removeItem('firstname');
                    localStorage.removeItem('username');
                    this.$router.push('/login')
                });
        }
    }
};
