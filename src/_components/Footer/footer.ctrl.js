export default {
    name: 'bottomfooter',

    mounted () {
        this.$store.dispatch('footer/getOffices');
        this.$store.dispatch('footer/getSocialNetworks');
    },

    computed: {
        menu: {
            get: function () {
                let _menu = this.$store.getters['footer/menu'];
                return _menu;
            },
            set: function (newValue) {}
        }
    }

};