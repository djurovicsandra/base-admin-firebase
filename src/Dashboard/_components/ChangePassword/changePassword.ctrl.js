import firebase from "firebase";
import {required} from 'vuelidate/lib/validators'

export default {
    data() {
        return {
            imageUrl: `${process.env.ASSETS_URL}/images`,
            userData: {
                password: ''
            },
            error: null
        };
    },
    validations: {
        userData: {
            password: {required},
        }
    },
    computed: {
        userData: {
            name: localStorage.getItem('firstname') || null,
            username: localStorage.getItem('username') || null,
        },
        passwordErrors() {
            const errors = [];
            if (!this.$v.userData.password.$dirty) return errors;
            !this.$v.userData.password.required && errors.push('Password is required');
            // !this.$v.userData.password.minLength && errors.push('Minimum password length is 6 characters');
            // !this.$v.userData.password.maxLength && errors.push('Maximum password length is 16 characters');
            return errors
        }
    },
    methods: {
        submit() {
            let _this = this;
            let user = firebase.auth().currentUser;
            user.updatePassword(this.userData.password)
                .then(function () {
                    alert('Password updated successfully!');
                    firebase.auth().signOut()
                        .then(() => {
                            localStorage.removeItem('firstname');
                            localStorage.removeItem('username');
                            _this.$router.push('/login');
                        });
                }, function (error) {
                    // An error happened.
                });
        }
    }
};

