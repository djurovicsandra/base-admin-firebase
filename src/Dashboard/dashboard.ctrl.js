import firebase from "firebase";
import {required, email, minLength, maxLength} from 'vuelidate/lib/validators'

export default {
    data() {
        return {
            imageUrl: `${process.env.ASSETS_URL}/images`,
            userData: {
                password: '',
                name: localStorage.getItem('firstname') || null,
                username: localStorage.getItem('username') || null,
            },
            error: null
        };
    },
    validations: {
        userData: {
            password: {required, minLength: minLength(6), maxLength: maxLength(16)},
            name: {required},
            username: {required, email},
        }
    },
    computed: {
        nameErrors() {
            const errors = [];
            if (!this.$v.userData.name.$dirty) return errors;
            !this.$v.userData.name.required && errors.push('Name is required');
            return errors
        },
        usernameErrors() {
            const errors = [];
            if (!this.$v.userData.username.$dirty) return errors;
            !this.$v.userData.username.required && errors.push('Username is required');
            !this.$v.userData.username.email && errors.push('Username must be in valid format');
            return errors
        },
        passwordErrors() {
            const errors = [];
            if (!this.$v.userData.password.$dirty) return errors;
            !this.$v.userData.password.required && errors.push('Password is required');
            !this.$v.userData.password.minLength && errors.push('Minimum password length is 6 characters');
            !this.$v.userData.password.maxLength && errors.push('Maximum password length is 16 characters');
            return errors
        }
    },
    mounted() {
    },
    methods: {
        submit() {
            // let user = firebase.auth().currentUser;
            // // Updates the user attributes:
            // user.updateProfile({
            //     displayName: this.userData.name,
            //     photoURL: ''
            // }).then(function () {
            //     alert('Profile updated successfully!');
            // }, function (error) {
            //     // An error happened.
            // });

            let _this = this;
            let user = firebase.auth().currentUser;
            user.updatePassword(this.userData.password)
                .then(function () {
                    alert('Password updated successfully!');
                    firebase.auth().signOut()
                        .then(() => {
                            localStorage.removeItem('firstname');
                            localStorage.removeItem('username');
                            _this.$router.push('/login');
                        });
                }).catch(err => {
                this.error = err.message;
            });
        }
    }
};

