import seo from './app-seo.js'
import header from './_components/Header/header.js'
import home from './Home/home.js'
import projects from './Projects/projects.js'
import project from './Project/project.js'
import footer from './_components/Footer/footer.js'


export default class StoreResource {
    Store(Vuex) {
        
        return {
            store: new Vuex.Store({
                modules: {
                    seo,
                    header,
                    home,
                    projects,
                    project,
                    footer
                }
            })
        }
    }
}