import {API_URL} from '../app-axios.js'

const api_path = '/projects?lang=en';

const state = {
  seo: {
    title: 'Projects Title',
    description: 'Projects description'
  },
  items: [],
  loadingStatus: ''
}

// getters
const getters = {
  items: (state, getters) => {
    return state.items;
  },
  loadingStatus: (state) => {
    return state.loadingStatus; 
  },
  seo: (state) => {
    return state.seo;
  }
}

// actions
const actions = {
  seo({commit}) {
    commit('seo/SET_SEO', state.seo, {root: true})
  },

  getProjects({commit}, data) {
    commit('SET_STATUS', 'LOADING')
    API_URL.get(api_path)
      .then(response => (
          commit('SET_STATUS', 'LOADED'),
          setTimeout(function(){
            commit('SET_STATUS', 'READY')
          }, 1000),

          commit('SET_ITEMS', response)
      )
    )
    .catch(error => (
        commit('SET_STATUS', error)
      )
    )
  }
}

// mutations
const mutations = {
  SET_STATUS (state, data) {
    // console.log('loadingStatus: ' + data);
    state.loadingStatus = data;
  },

  SET_ITEMS (state, data) {
    state.items = data.data;
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}