import {SEOResource} from '../app-seo.js'

export default {
    name: 'projects',
    
    data () {
        return {
            
        }
    },

    computed: {
        items: { 
            get: function() {
                return this.$store.getters['projects/items']
            },
            set: function() {}
        }
    },

    mounted () {
        this.$store.dispatch('projects/seo');
        this.$store.dispatch('projects/getProjects');
    },

    metaInfo () {
        return new SEOResource().SEOData(this);
    }
};