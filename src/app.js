import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import Meta from 'vue-meta'
import Vuetify from 'vuetify'
import Vuelidate from 'vuelidate'

import routes from './app-routes'
import StoreResource from './app-store'
import * as firebase from 'firebase/app';
import './assets/style/style.scss'

// Components
import Header from './_components/Header/header.vue'
import Footer from './_components/Footer/footer.vue'

const router = new VueRouter({
  mode: 'history',
  routes, // short for `routes: routes`
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
});

Vue.use(VueRouter);
Vue.use(Meta);
Vue.use(Vuex);
Vue.use(Vuetify);
Vue.use(Vuelidate);

Vue.component('app-footer', Footer);
Vue.component('app-header', Header);

Vue.config.productionTip = false;

const configOptions = {
  apiKey: "AIzaSyAGh4oEX6e4zvQvpYOYRMqP48tKFN0QdC8",
  authDomain: "base-admin-59915.firebaseapp.com",
  databaseURL: "https://base-admin-59915.firebaseio.com",
  projectId: "base-admin-59915",
  storageBucket: "base-admin-59915.appspot.com",
  messagingSenderId: "138285642044",
  appId: "1:138285642044:web:c58cdc9ae08b4bf1aa6e39",
  measurementId: "G-JZM3YZXMXM"
};

firebase.initializeApp(configOptions);

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  
  if (requiresAuth && !currentUser) next('login');
  else if (!requiresAuth && currentUser) next ('dashboard');
  else next();
});

const store = new StoreResource().Store(Vuex).store;

let app = '';
firebase.auth().onAuthStateChanged(() => {
  if(!app) {
    app = new Vue({
      router,
      store,
      vuetify: new Vuetify({
        theme: {
          light: true,
        }
      })
    }).$mount('#app');
  }
})
