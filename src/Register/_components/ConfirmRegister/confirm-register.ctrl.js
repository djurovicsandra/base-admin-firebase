import firebase from "firebase";
import {required, email, minLength, maxLength} from 'vuelidate/lib/validators'

export default {
    data() {
        return {
            imageUrl: `${process.env.ASSETS_URL}/images`,
            userData: {
                firstName: '',
                lastName: '',
                username: '',
                password: '',
            },
            error: null
        };
    },
    validations: {
        userData: {
            firstName: {required},
            lastName: {required},
            username: {required, email},
            password: {required, minLength: minLength(6), maxLength: maxLength(16)}
        }
    },
    computed: {
        firstNameErrors() {
            const errors = [];
            if (!this.$v.userData.firstName.$dirty) return errors;
            !this.$v.userData.firstName.required && errors.push('First name is required');
            return errors
        },
        lastNameErrors() {
            const errors = [];
            if (!this.$v.userData.lastName.$dirty) return errors;
            !this.$v.userData.lastName.required && errors.push('Last name is required');
            return errors
        },
        usernameErrors() {
            const errors = [];
            if (!this.$v.userData.username.$dirty) return errors;
            !this.$v.userData.username.required && errors.push('Username is required');
            !this.$v.userData.username.email && errors.push('Username must be in valid format');
            return errors
        },
        passwordErrors() {
            const errors = [];
            if (!this.$v.userData.password.$dirty) return errors;
            !this.$v.userData.password.required && errors.push('Password is required');
            !this.$v.userData.password.minLength && errors.push('Minimum password length is 6 characters');
            !this.$v.userData.password.maxLength && errors.push('Maximum password length is 16 characters');
            return errors
        }
    },
    mounted() {
    },
    methods: {
        async submit() {
            let _this = this;
            if (firebase.auth().isSignInWithEmailLink(window.location.href)) {
                let email = _this.userData.username;
                await firebase.auth().signInWithEmailLink(email, window.location.href)
                    .then(function (result) {
                        localStorage.setItem('firstname', _this.userData.firstName);
                        localStorage.setItem('username', _this.userData.username);

                        firebase.database().ref('users/' + result.user.uid).set({
                            first_name: _this.userData.firstName,
                            last_name: _this.userData.lastName,
                        }).then(function () {
                            alert('Data successfully changed');
                        }, function (error) {
                            // alert('Error saving data')
                        });

                        result.user.updateProfile({
                            displayName: _this.userData.firstName,
                        }).then(function () {
                        }, function (error) {
                            // An error happened.
                        });

                        result.user.updatePassword(_this.userData.password)
                            .then(function () {
                                // firebase.analytics().setUserProperties({first_name: _this.userData.firstName});
                                // firebase.analytics().setUserProperties({first_name: _this.userData.lastName});
                            }, function (error) {
                            });

                        _this.$router.push('/dashboard');
                    })
                    .catch(function (error) {
                    });
            }
        },
        toLogin() {
            this.$router.push('/login')
        }
    }
};
