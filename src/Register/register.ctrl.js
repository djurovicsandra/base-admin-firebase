import firebase from "firebase";
import {required, email} from 'vuelidate/lib/validators'

export default {
    data() {
        return {
            imageUrl: `${process.env.ASSETS_URL}/images`,
            userData: {
                name: '',
                username: '',
                password: '',
            },
            error: null
        };
    },
    validations: {
        userData: {
            // name: {required},
            username: {required, email},
            // password: {required}
        }
    },
    computed: {
        nameErrors() {
            const errors = [];
            if (!this.$v.userData.name.$dirty) return errors;
            !this.$v.userData.name.required && errors.push('Name is required');
            return errors
        },
        usernameErrors() {
            const errors = [];
            if (!this.$v.userData.username.$dirty) return errors;
            !this.$v.userData.username.required && errors.push('Email is required');
            !this.$v.userData.username.email && errors.push('Email must be in valid format');
            return errors
        },
        passwordErrors() {
            const errors = [];
            if (!this.$v.userData.password.$dirty) return errors;
            !this.$v.userData.password.required && errors.push('Password is required');
            // !this.$v.userData.password.minLength && errors.push('Minimum password length is 6 characters');
            // !this.$v.userData.password.maxLength && errors.push('Maximum password length is 16 characters');
            return errors
        }
    },
    methods: {
        submit() {
            // firebase
            //     .auth()
            //     .createUserWithEmailAndPassword(this.userData.username, this.userData.password)
            //     .then(data => {
            //         data.user.updateProfile({
            //                 displayName: this.userData.name
            //             })
            //             .then(() => {
            //                 localStorage.setItem('firstname',this.userData.name);
            //                 localStorage.setItem('username', data.user.email);
            //                 this.$router.push('/dashboard')
            //             });
            //     })
            //     .catch(err => {
            //         this.error = err.message;
            //     });

            const actionCodeSettings = {
                // URL you want to redirect back to. The domain (www.example.com) for this
                // URL must be whitelisted in the Firebase Console.
                url: 'http://127.0.0.1/confirm-registration',
                // This must be true.
                handleCodeInApp: true,
            };

            localStorage.setItem('emailForSignIn', this.userData.username);
            alert('You will receive an email with the confirmation link.');
            return firebase.auth().sendSignInLinkToEmail(this.userData.username, actionCodeSettings);
        },
        toLogin() {
            this.$router.push('/login')
        }
    }
};
