import App from './app.vue'
import PageNotFound from './PageNotFound/pageNotFound.vue'

import Login from './Login/login.vue'
import Register from './Register/register.vue'
import Dashboard from './Dashboard/dashboard.vue'
import ForgotPassword from './Login/_components/ForgotPassword/forgotPassword.vue'
import FullLayout from './_components/FullLayout/fullLayout.vue'
import SimpleLayout from './_components/SimpleLayout/simpleLayout.vue'
import ConfirmRegister from './Register/_components/ConfirmRegister/confirm-register.vue'

const default_lang = process.env.DEFAULT_LANGUAGE;

let routes = [
    { path: '', component: App, children: [
            {
                path: '',
                component: SimpleLayout,
                children: [
                    {path: '', redirect: 'login'},
                    {path: 'login', component: Login, name: 'login'},
                    {path: 'forgot-password', component: ForgotPassword, name: 'forgot-password'},
                    {path: 'register', component: Register, name: 'register'},
                    {path: 'confirm-registration', component: ConfirmRegister, name: 'confirm-registration'},
                ],
            },
            {
                path: '',
                component: FullLayout,
                children: [
                    {path: '', redirect: 'dashboard'},
                    {path: 'dashboard', component: Dashboard, name: 'dashboard',
                        meta: {
                            requiresAuth: true
                        }}
                ],
            },
            {
                path: '*',
                component: PageNotFound,
                name: '404',
                meta: {
                    requiresVisitor: true
                }
            }
        ]
    },
    { path: '*', redirect: '/login' },
];

export default routes


// EXAMPLE:
// const routes = [
//     { path: '/login', component: LoginView},
//     { path: '/', component: DashView, auth: true,
//       children: [
//         { path: '', component: DashboardView, name: 'Dashboard' },
//         { path: '/tables', component: TablesView, name: 'Tables',
//       ]
//     },
//     { path: '*', component: NotFoundView }
//   ]
// export default routes
